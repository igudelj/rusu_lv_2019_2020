from sklearn.preprocessing import PolynomialFeatures
import numpy as np
import matplotlib.pyplot as plt 
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

def plot_confusion_matrix(c_matrix):

 norm_conf = []
 for i in c_matrix:
     a = 0
     tmp_arr = []
     a = sum(i, 0)
     for j in i:
         tmp_arr.append(float(j)/float(a))
     norm_conf.append(tmp_arr)
 fig = plt.figure()
 ax = fig.add_subplot(111)
 res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
 width = len(c_matrix)
 height = len(c_matrix[0])
 for x in range(width):
     for y in range(height):
         ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                         horizontalalignment='center',
                         verticalalignment='center', color = 'green', size = 20)
 fig.colorbar(res)
 numbers = '0123456789'
 plt.xticks(range(width), numbers[:width])
 plt.yticks(range(height), numbers[:height])

 plt.ylabel('Stvarna klasa')
 plt.title('Predvideno modelom')
 plt.show()

NUMBER_OF_TRAIN_DATA_SPECIMENS = 200
NUMBER_OF_TEST_DATA_SPECIMENS = 100
np.random.seed(242)
random_trained_data = generate_data(NUMBER_OF_TRAIN_DATA_SPECIMENS)

np.random.seed(12)
random_test_data = generate_data(NUMBER_OF_TEST_DATA_SPECIMENS)

poly = PolynomialFeatures(degree=3, include_bias = False)
X_train = poly.fit_transform(random_trained_data[:,0:2])
Y_train = random_trained_data[:,-1]


X_test= poly.fit_transform(random_test_data[:,0:2])
Y_test= random_test_data[:,-1]

logRegr = LogisticRegression(max_iter = 500)
logRegr.fit(X_train,Y_train)

param = list(logRegr.coef_[0])
param.append(logRegr.intercept_[0])

for i in range (0,len(random_trained_data)):
    if(random_trained_data[i,2]<1):
        color= 'lightblue'
        plt.scatter(X_train[i,0],X_train[i,1],s=50,c=color,marker='D',edgecolors='black')
    elif(random_trained_data[i,2]>=0):
        color= 'red'
        plt.scatter(X_train[i,0],X_train[i,1],s=50,c=color,marker='h',edgecolors='black')
        

f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(random_trained_data[:,0])-0.5:max(random_trained_data[:,0])+0.5:.05,
 min(random_trained_data[:,1])-0.5:max(random_trained_data[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
grid = poly.fit_transform(grid)
probs = logRegr.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
plt.show()

y_predicted = logRegr.predict(X_test)

plt.figure()
for i in range(0,len(y_predicted)):
    if(y_predicted[i] == Y_test[i]):
        color = 'green'
        plt.scatter(X_test[i,0],X_test[i,1],s=50,c=color,marker='D',edgecolors='black')
    elif(y_predicted[i] != Y_test[i]):
        color= 'black'
        plt.scatter(X_test[i,0],X_test[i,1],s=50,c=color,marker='h',edgecolors='black')

confMatrix = confusion_matrix(Y_test, y_predicted)
print("Accuracy= ",(confMatrix[0,0]+confMatrix[1,1])/(confMatrix[0,0]+confMatrix[0,1]+confMatrix[1,0]+confMatrix[1,1]))
print("Missclasification rate= ",1-(confMatrix[0,0]+confMatrix[1,1])/(confMatrix[0,0]+confMatrix[0,1]+confMatrix[1,0]+confMatrix[1,1]))
print("Precision= ",confMatrix[0,0]/(confMatrix[0,0]+confMatrix[0,1]))
print("Sensitivity= ",confMatrix[0,0]/(confMatrix[0,0]+confMatrix[1,0]))
print("Specificity= ",confMatrix[1,1]/(confMatrix[1,1]+confMatrix[0,1]))

plot_confusion_matrix(confMatrix)


