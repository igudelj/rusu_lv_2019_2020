import numpy as np
import matplotlib.pyplot as plt 
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix

def plot_confusion_matrix(c_matrix):

 norm_conf = []
 for i in c_matrix:
     a = 0
     tmp_arr = []
     a = sum(i, 0)
     for j in i:
         tmp_arr.append(float(j)/float(a))
     norm_conf.append(tmp_arr)
 fig = plt.figure()
 ax = fig.add_subplot(111)
 res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
 width = len(c_matrix)
 height = len(c_matrix[0])
 for x in range(width):
     for y in range(height):
         ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                         horizontalalignment='center',
                         verticalalignment='center', color = 'green', size = 20)
 fig.colorbar(res)
 numbers = '0123456789'
 plt.xticks(range(width), numbers[:width])
 plt.yticks(range(height), numbers[:height])

 plt.ylabel('Stvarna klasa')
 plt.title('Predvideno modelom')
 plt.show()

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

NUMBER_OF_TRAIN_DATA_SPECIMENS = 200
NUMBER_OF_TEST_DATA_SPECIMENS = 100
np.random.seed(242)
random_trained_data = generate_data(NUMBER_OF_TRAIN_DATA_SPECIMENS)

np.random.seed(12)
random_test_data = generate_data(NUMBER_OF_TEST_DATA_SPECIMENS)

X_train = random_trained_data[:,:-1]
Y_train = random_trained_data[:,-1]

X_test= random_test_data[:,:-1]
Y_test= random_test_data[:,-1]

logRegr = LogisticRegression()
logRegr.fit(X_train,Y_train)
       

y_predicted = logRegr.predict(X_test)
confMatrix = confusion_matrix(Y_test, y_predicted)

print("Accuracy= ",(confMatrix[0,0]+confMatrix[1,1])/(confMatrix[0,0]+confMatrix[0,1]+confMatrix[1,0]+confMatrix[1,1]))
print("Missclasification rate= ",1-(confMatrix[0,0]+confMatrix[1,1])/(confMatrix[0,0]+confMatrix[0,1]+confMatrix[1,0]+confMatrix[1,1]))
print("Precision= ",confMatrix[0,0]/(confMatrix[0,0]+confMatrix[0,1]))
print("Sensitivity= ",confMatrix[0,0]/(confMatrix[0,0]+confMatrix[1,0]))
print("Specificity= ",confMatrix[1,1]/(confMatrix[1,1]+confMatrix[0,1]))
plot_confusion_matrix(confMatrix)



        




