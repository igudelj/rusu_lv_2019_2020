from sklearn import datasets
import numpy as np
import pandas as pd
from scipy.cluster.hierarchy import dendrogram, linkage
import matplotlib.pyplot as plt 
from sklearn.cluster import KMeans



class K_Means:
    def __init__(self, k=5, tol=0.001, max_iter=300):
        self.k = k
        self.tol = tol
        self.max_iter = max_iter

    def fit(self,data):

        self.centroids = {}

        for i in range(self.k):
            self.centroids[i] = data[i]

        for i in range(self.max_iter):
            self.classifications = {}

            for i in range(self.k):
                self.classifications[i] = []

            for featureset in data:
                distances = [np.linalg.norm(featureset-self.centroids[centroid]) for centroid in self.centroids]
                classification = distances.index(min(distances))
                self.classifications[classification].append(featureset)

            prev_centroids = dict(self.centroids)

            for classification in self.classifications:
                self.centroids[classification] = np.average(self.classifications[classification],axis=0)

            optimized = True

            for c in self.centroids:
                original_centroid = prev_centroids[c]
                current_centroid = self.centroids[c]
                print(original_centroid)
              #  plt.plot(original_centroid[c,0],current_centroid[c,1],'k-',lw=1)
                if np.sum((current_centroid-original_centroid)/original_centroid*100.0) > self.tol:
                    print(np.sum((current_centroid-original_centroid)/original_centroid*100.0))
                    optimized = False

            if optimized:
                break

    def predict(self,data):
        distances = [np.linalg.norm(data-self.centroids[centroid]) for centroid in self.centroids]
        classification = distances.index(min(distances))
        return classification

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          centers=4,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X
#PRVI ZADATAK
#---------------------------------------------------------------

np.random.seed() 
data_train=generate_data(500,1) 

numOfClusters = int(input("Unesite broj klastera: (Max 10): "))
km = KMeans(n_clusters=numOfClusters
            , init='random',n_init=10, max_iter=300, tol=1e-04, random_state=0)
y_kmeans = km.fit_predict(data_train)

NoviKM = K_Means()
NoviKM.fit(data_train)


colors = ['lightblue', 'green', 'red','yellow','aqua','darkorange', 'lime','azure','magenta','indigo']
markers=['.','o','h','*','d','<','>','s','p','^']
markCounter=0
colorCounter=0
for i in range(0,numOfClusters):
    if(colorCounter == numOfClusters or markCounter== numOfClusters):
        markCounter = 0
        colorCounter= 0
    plt.scatter(data_train[y_kmeans==i,0],data_train[y_kmeans == i,1],s=50,c=colors[colorCounter],marker=markers[markCounter],edgecolors='black')
    markCounter = markCounter+1
    colorCounter = colorCounter+1
print(km.cluster_centers_)
# plot the centroids

plt.scatter(
    km.cluster_centers_[:, 0], km.cluster_centers_[:, 1],
    s=100, marker='D',
    c='gold', edgecolor='black',
    label='centroids'
)
plt.legend(scatterpoints=1)
plt.grid()
plt.show()
#---------------------------------------------------------------


