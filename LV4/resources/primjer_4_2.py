import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures

def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

MSE_test=[None]*3
MSE_train=[None]*3

x = np.linspace(1,10,50)
y_true = non_func(x)
y_measured = add_noise(y_true)

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

# make polynomial features
poly = PolynomialFeatures(degree=15)
xnew = poly.fit_transform(x)
    
np.random.seed(12)
indeksi = np.random.permutation(len(xnew))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnew)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnew)))+1:len(xnew)]

xtrain = xnew[indeksi_train,]
ytrain = y_measured[indeksi_train]

xtest = xnew[indeksi_test,]
ytest = y_measured[indeksi_test]

linearModel15 = lm.LinearRegression()
linearModel15.fit(xtrain,ytrain)

ytest_p = linearModel15.predict(xtest)
MSE_test[0]=mean_squared_error(ytest, ytest_p)
#MSE_train[0]=mean_squared_error(ytrain, ytest_p)


plt.figure(1)
plt.plot(xtest[:,1],ytest_p,'og',label='predicted')
plt.plot(xtest[:,1],ytest,'or',label='test')
plt.legend(loc = 4)

#pozadinska funkcija vs model
plt.figure(2)
plt.plot(x,y_true,label='f')
plt.plot(x, linearModel15.predict(xnew),'r-',label='model')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(xtrain[:,1],ytrain,'ok',label='train')
plt.legend(loc = 4)


#stupanj=2
poly2 = PolynomialFeatures(degree=2)
xnew2 = poly2.fit_transform(x)
        
np.random.seed(12)
indeksi = np.random.permutation(len(xnew2))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnew2)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnew2)))+1:len(xnew2)]
    
xtrain = xnew2[indeksi_train,]
ytrain = y_measured[indeksi_train]
    
xtest = xnew2[indeksi_test,]
ytest = y_measured[indeksi_test]
    
linearModel2 = lm.LinearRegression()
linearModel2.fit(xtrain,ytrain)
    
ytest_p = linearModel2.predict(xtest)
MSE_test[1]=mean_squared_error(ytest, ytest_p)
#MSE_train[1]=mean_squared_error(ytrain, ytest_p)

#stupanj=6
poly6 = PolynomialFeatures(degree=6)
xnew6 = poly6.fit_transform(x)
        
np.random.seed(12)
indeksi = np.random.permutation(len(xnew6))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnew6)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnew6)))+1:len(xnew6)]
    
xtrain = xnew6[indeksi_train,]
ytrain = y_measured[indeksi_train]
    
xtest = xnew6[indeksi_test,]
ytest = y_measured[indeksi_test]
    
linearModel6 = lm.LinearRegression()
linearModel6.fit(xtrain,ytrain)
    
ytest_p = linearModel6.predict(xtest)
MSE_test[2]=mean_squared_error(ytest, ytest_p)
#MSE_train[2]=mean_squared_error(ytrain, ytest_p)

print(MSE_test)
#print(MSE_train)

plt.figure(3)
plt.plot(x,y_true,label='f')
plt.plot(x, linearModel2.predict(xnew2),'g-',label='model, deg=2')
plt.plot(x, linearModel6.predict(xnew6),'r-',label='model, deg=6')
plt.plot(x, linearModel15.predict(xnew),'y-',label='model, deg=15')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(xtrain[:,1],ytrain,'ok',label='train')
plt.legend(loc = 4)
